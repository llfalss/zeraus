jQuery(document).ready(function($) {
  $('.ht-mask__money').mask('#.##0,00', {reverse: true, placeholder : "0,00"});
  $('.ht-mask__date').mask("00/00/0000", {placeholder: "__/__/____"});
  $('.ht-mask__cnpj').mask('00.000.000/0000-00');
  $('.ht-mask__cpf').mask('000.000.000-00');
  $('.ht-mask__cep').mask("00000-000", {placeholder: "_____-___"});
  var optionsPhone = {onKeyPress : function(val){}}
  var SPMaskBehavior = function (val) { return val.replace(/\D/g, '').length === 11 ? '(00) 0 0000-0000' : '(00) 0000-00009'; }, spOptions = { placeholder: "(XX) X XXXX-XXXX", onKeyPress: function(val, e, field, options) { field.mask(SPMaskBehavior.apply({}, arguments), options); } };
  $('.ht-mask__cel').mask(SPMaskBehavior, spOptions);
  $(".ht-gallery--js").lightGallery();
  $(".ht-image__wrapper--js").lightGallery();
  

  $(".ht-header__control--js").on("click", function(){
    $("body").toggleClass("ht-header__nav--active");
  });
  $(".home-slider--js").slick({
    prevArrow : "<div class='home-slider__nav home-slider__nav--prev'><i class='fas fa-chevron-left'></i></div>",
    nextArrow : "<div class='home-slider__nav home-slider__nav--next'><i class='fas fa-chevron-right'></i></div>",
  });
  $(".ht-testimonal--js").slick({
    nextArrow : "<div class=\"testimonal__arrow testimonal__arrow--next\">Próximo</div>",
    prevArrow : "<div class=\"testimonal__arrow testimonal__arrow--prev\">Anterior</div>",
  });
  $(window).scroll(function(){
    var credit = $(".ht-footer__items").offset().top;
    var pageTop = $(window).scrollTop();
    var pageBottom = pageTop + $(window).height();

    if(pageBottom >= credit){
      $("body").addClass("ht-wpp__hide");
    }else{
      $("body").removeClass("ht-wpp__hide");
    }
  });
  $(".ht-mobile__subitem").hide();
  $(".ht-nav__mobile--js").on("click", function(){
    $("html").toggleClass("ht-nav__mobile--active");
  })
  $(".ht-nav__mobile--dropdown").on("click", function(e){
    $(this).find(".ht-mobile__subitem").slideToggle();
    $(this).toggleClass("ht-mobile__subitem--active");
  });
  $(".ht-home-banner--js").slick({
    prevArrow : null,
    nextArrow : null,
    dots : true,
    autoplay : true,
    pauseOnHover : true,
  });
  $(".modal__wrapper").hide();
  $(".modal__show").on("click", function(e){
    e.preventDefault();
    var modal = "."+ $(this).attr("data-id");
    $(modal).fadeIn();
    $(modal).css({
      display : "flex"
    });
    $("body").css({
      overflow : "hidden"
    });
  });
  $(".modal__close").on("click", function(e){
    e.preventDefault();
    var modal = "."+ $(this).attr("data-id");
    $(modal).fadeOut();
    $("body").css({
      overflow : "auto"
    });
  });
});
