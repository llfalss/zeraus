<?php
//Template name: Quem somos
get_header();
get_template_part("template/about","content");
get_template_part("template/global","gallery");
get_template_part("template/global","produtos");
get_template_part("template/global","testimonal");
get_template_part("template/global","header");
get_template_part("template/global","contact");
get_template_part("template/global","footer");
get_template_part("template/global","footer-nav");
get_footer();
