<?php
$image = get_field("ht_options_contact_image", "options");
$title = get_field("ht_options_contact_title", "options");
?>
<section class="ht-section__double ht-section__double--mobile-text-first ht-section__double--desktop-image-first ht-contact">
  
  <div class="ht-section__image" style="background-image:url('<?php print $image["url"] ?>')">
    <?php get_template_part("template/global","map"); ?>
  </div>
  <div class="ht-section__text">
    <div class="ht-section__wrapper ">
      <h2 class="ht-title ht-title__simple"><?= ($title ?? "Fale conosco") ?></h2>
      <form class="ht-contact__form" method="post">
      <div class="ht-contact__group ht-contact__group--one-line">
        <label class="ht-contact__label">
          Nome Completo
          <input
          type="text"
          name="<?php print md5("ht-nome") ?>"
          class="ht-contact__field"
          placeholder="Ex: Antônio Rodrigues"
          required>
        </label>
      </div>
      <div class="ht-contact__group">
        <label class="ht-contact__label">
          E-mail
          <input
          type="email"
          name="<?php print md5("ht-email") ?>"
          class="ht-contact__field"
          placeholder="Ex: exemplo@gmail.com"
          required>
        </label>
      </div>
      <div class="ht-contact__group">
        <label class="ht-contact__label">
          Telefone
          <input
          type="tel"
          name="<?php print md5("ht-telefone") ?>"
          class="ht-contact__field ht-mask__cel"
          placeholder="Ex: exemplo@gmail.com"
          required>
        </label>
      </div>
      <div class="ht-contact__group ht-contact__group--one-line">
        <label class="ht-contact__label">
          Mensagem
          <textarea
          name="<?php print md5("ht-mensagem") ?>"
          class="ht-contact__field ht-contact__field--textarea"
          cols="8"></textarea>
        </label>
      </div>
      <div class="ht-contact__group ht-contact__group--submit">
        <button
        type="submit"
        class="ht-button ht-contact__submit">Enviar formulário      
<svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M22.3185 11.5573L4.61021 3.22401C4.43189 3.1401 4.23343 3.1085 4.03788 3.13288C3.84232 3.15725 3.65769 3.2366 3.50542 3.3617C3.35315 3.48681 3.2395 3.65254 3.17766 3.83966C3.11581 4.02678 3.10832 4.2276 3.15604 4.4188L4.41854 9.46984L12.4998 12.5001L4.41854 15.5303L3.15604 20.5813C3.10742 20.7726 3.11429 20.9739 3.17585 21.1615C3.2374 21.3491 3.3511 21.5152 3.50364 21.6406C3.65619 21.7659 3.84126 21.8452 4.03722 21.8692C4.23319 21.8932 4.43193 21.8609 4.61021 21.7761L22.3185 13.4428C22.4975 13.3586 22.6488 13.2253 22.7548 13.0584C22.8608 12.8914 22.9171 12.6978 22.9171 12.5001C22.9171 12.3023 22.8608 12.1087 22.7548 11.9417C22.6488 11.7748 22.4975 11.6415 22.3185 11.5573Z" fill="white"/>
</svg>
</button>
        <input type="hidden" name="<?php print md5("ht-action") ?>" value="<?php print md5("ht-contato") ?>">
      </div>
    </form>
    </div>
  </div>
</section>

