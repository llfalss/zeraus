<?php
$logo = ht_get_logo();
$contatos = ht_get_contact();
$social = ht_get_social();
$nav = ht_get_nav();
?>
<div class="ht-footer">
  <div class="ht-footer__logo">
    <?php if(!empty($logo["main"])): ?>
      <img src="<?php print $logo["main"]["url"]; ?>" class="ht-footer__logo--image" alt="Logo <?php print bloginfo("name"); ?>">
    <?php else: ?>
      <span class="ht-footer__logo--text"><?php print bloginfo("name"); ?></span>
    <?php endif; ?>
  </div>

 
  <div class="ht-footer__nav">
    <?php if(!empty($nav["nav"])): ?>
      <?php foreach($nav["nav"] as $n): ?>
        <div  class="ht-ht-footer__nav--item">
          <a href="<?php print $n["url"] ?>" class="ht-ht-footer__nav--label">
            <?php print $n["label"] ?>
          </a>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>

  <div class="ht-footer__social">
    <?php if(!empty($social)): ?>
      <?php if(!empty($social["facebook"])): ?>
        <a href="<?php print $social["facebook"]; ?>" class="ht-footer__social--item">
          <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
            <path d="M25.725 0H3.675C1.64824 0 0 1.68187 0 3.75V26.25C0 28.3181 1.64824 30 3.675 30H25.725C27.7518 30 29.4 28.3181 29.4 26.25V3.75C29.4 1.68187 27.7518 0 25.725 0Z" fill="#3C3C3C"/>
            <path d="M24.8066 15H20.2129V11.25C20.2129 10.215 21.0361 10.3125 22.0504 10.3125H23.8879V5.625H20.2129C17.1682 5.625 14.7004 8.14313 14.7004 11.25V15H11.0254V19.6875H14.7004V30H20.2129V19.6875H22.9691L24.8066 15Z" fill="white"/>
            </g>
            <defs>
            <clipPath id="clip0">
            <rect width="29.4" height="30" fill="white"/>
            </clipPath>
            </defs>
          </svg>
        </a>
      <?php endif; ?>
      <?php if(!empty($social["instagram"])): ?>
        <a href="<?php print $social["instagram"]; ?>" class="ht-footer__social--item">
          <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clip-path="url(#clip0)">
            <path d="M21.4594 0H9.13985C4.43074 0 0.599609 3.90932 0.599609 8.71453V21.2855C0.599609 26.0907 4.43074 30 9.13985 30H21.4594C26.1685 30 29.9996 26.0907 29.9996 21.2855V8.71453C29.9996 3.90932 26.1685 0 21.4594 0ZM27.7027 21.2855C27.7027 24.7983 24.902 27.6562 21.4594 27.6562H9.13985C5.69724 27.6562 2.89649 24.7983 2.89649 21.2855V8.71453C2.89649 5.20166 5.69724 2.34375 9.13985 2.34375H21.4594C24.902 2.34375 27.7027 5.20166 27.7027 8.71453V21.2855Z" fill="#3C3C3C"/>
            <path d="M15.2992 6.91406C10.9298 6.91406 7.375 10.5414 7.375 15C7.375 19.4586 10.9298 23.0859 15.2992 23.0859C19.6687 23.0859 23.2234 19.4586 23.2234 15C23.2234 10.5414 19.6687 6.91406 15.2992 6.91406ZM15.2992 20.7422C12.1963 20.7422 9.67188 18.1662 9.67188 15C9.67188 11.8338 12.1963 9.25781 15.2992 9.25781C18.4021 9.25781 20.9266 11.8338 20.9266 15C20.9266 18.1662 18.4021 20.7422 15.2992 20.7422Z" fill="#3C3C3C"/>
            <path d="M23.3389 7.96875C23.9731 7.96875 24.4873 7.44408 24.4873 6.79688C24.4873 6.14967 23.9731 5.625 23.3389 5.625C22.7046 5.625 22.1904 6.14967 22.1904 6.79688C22.1904 7.44408 22.7046 7.96875 23.3389 7.96875Z" fill="#3C3C3C"/>
            </g>
            <defs>
            <clipPath id="clip0">
            <rect width="29.4" height="30" fill="white" transform="translate(0.599609)"/>
            </clipPath>
            </defs>
          </svg>
        </a>
      <?php endif; ?>
      <?php if(!empty($social["youtube"])): ?>
        <a href="<?php print $social["instagram"]; ?>" class="ht-footer__social--item">
          <i class="fab fa-youtube ht-footer__social--icon"></i>
        </a>
      <?php endif; ?>
    <?php endif; ?>
  </div>


  <div class="ht-footer__items">
    <a href="https://hattrickcomunicacao.com.br" class="ht-footer__link ht-footer__link--hat-trick" target="_blank">
      Desenvolvido por
      <img
      src="<?= ht_get_theme_image("/image/logo-hat-trick.svg") ?>"
      alt="Logo Hat Trick Comunicação"
      class="ht-footer__image ht-footer__image--hat-trick"
      target="_blank">
    </a>
  </div>
</div>
<?php if(!empty($contatos["whatsapp"])): ?>
  <a href="<?php print $contatos["whatsapp"]["url"] ?>" class="ht-whatsapp">
    <i class="fab fa-whatsapp ht-whatsapp__icon"></i>
  </a>
<?php endif; ?>
