<?php 
$p = get_queried_object();
$gallery = get_field("ht_gallery", $p);
$count = 1;

if($gallery):
?>
<div class="ht-gallery ht-gallery--js">
    <?php foreach($gallery as $g): ?>
        <a 
        href="<?= $g["url"] ?>" 
        class="ht-gallery__item<?php
            if($count == 6) print " ht-gallery__item--more";
            if($count > 6) print " ht-gallery__item--hide";
        ?>" 
        style="background-image:url('<?= $g["sizes"]["large"] ?>')">
            <img src="<?= $g["sizes"]["thumbnail"] ?>" alt="" class="ht-gallery__image">
            <div class="ht-gallery__more">
                <div class="ht-more__icon"><i class="fas fa-plus"></i></div>
                <div class="ht-more__text">Veja mais</div>
            </div>
        </a>
        <?php $count++; ?>
    <?php endforeach; ?>
</div>
<?php endif; ?>