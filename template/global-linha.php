<?
    $produtos = get_field("ht_produto-group");
    $i = 0;
    $count1 = 0;
    $count2 = 0;
    $contato = ht_get_contact();
   
?>

<div class="ht__produtos-wraper">
    <h1><? single_post_title(); ?></h1>
    <? if(!empty($produtos)): ?>
        <div class="ht__produtos-list-wrapper">
            <?foreach ($produtos as $produto): 
            
                $i++;
                if(($i % 2)==0){ ?>
                <div class="ht__produto-card">
                        <div>
                            <h1><? print $produto["ht_product-name"]; ?></h1>
                            <p><? print $produto["ht_about-product"]; ?></p>
                            <a class="button-produto" href="<? print $contato["whatsapp"]["url"];  ?>">Fale Conosco</a>
                        </div>
                        <? if(!empty($produto["ht_product-gallery"])): ?>
                            <div class="gallery ht-gallery--js">
                                <? foreach($produto["ht_product-gallery"] as $image):
                                    $count1++    
                                ?>
                                <a  href="<? print $image ?>" style="background-image:url(<? print $image ?>); background-size: cover;" class="<?php
                                    if($count1 == 0) print " ht-galeria__item--thumb";
                                    if($count1 > 1) print " ht-galeria__item--hide";
                                    ?>">
                                </a>
                                <? endforeach; ?>
                            </div>
                        <? endif; ?>
                    </div>
                <? }else{ ?>
                    <div class="ht__produto-card">
                            <? if(!empty($produto["ht_product-gallery"])): ?>
                                <div class="gallery ht-gallery--js">
                                    <? foreach($produto["ht_product-gallery"] as $image):
                                        $count2++    
                                    ?>
                                        <a  href="<? print $image ?>" style="background-image:url(<? print $image ?>); background-size: cover;" class="<?php
                                            if($count2 == 0) print " ht-galeria__item--thumb";
                                            if($count2 > 1) print " ht-galeria__item--hide";
                                            ?>">
                                        </a>
                                    <? endforeach; ?>
                                </div>
                            <? endif; ?>

                        <div>
                            <h1><? print $produto["ht_product-name"]; ?></h1>
                            <p><? print $produto["ht_about-product"]; ?></p>
                            <a class="button-produto" href="<? print $contato["whatsapp"]["url"];  ?>">Fale Conosco</a>
                        </div>
                    </div>
                <? }; ?>
                
            <? endforeach; ?>  
        </div>
    <? else: ?>
        <h1 class="sem-produtos">
            Sem produtos ainda...
        </h1>
    <? endif; ?>
</div>