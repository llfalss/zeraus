<?php 
$map = get_field("ht_option_map", "option");
if(!empty($map)):
?>
<div class="ht-footer__map">
    <?= $map ?>
</div>
<?php endif; ?>