<?php
$title = get_field("ht_option_testimonal_title", "options");
$image = get_field("ht_option_testimonal_image", "options");
$content = get_field("ht_testimonal", "options");
if($content):
?>
<section class="ht-section__double ht-section__double--mobile-text-first ht-section__double--desktop-text-first ht-testimonal__wrapper">
  <div class="ht-section__image" <?php if(!empty($image)): ?>style="background-image:url('<?php print $image["url"] ?>')"<?php endif; ?>>

  </div>
  <div class="ht-section__text">
    <div class="ht-section__wrapper ">
      <h2 class="ht-title ht-title__simple"><?= $title ?></h2>
      <div class="ht-testimonal ht-testimonal--js">
        <?php foreach($content as $i => $testimonal): ?>
          <div class="ht-testimonal__item">
            <div class="ht-testimonal__text">
              <?php ht_set_modal("modal__{$i}", wpautop($testimonal["ht_testimonal_content"]), "ht-testimonal__more", 215 ); ?>
            </div>
            <div class="ht-testimonal__info">
              <div class="ht-testimonal__icon">
                <img src="<?= ht_get_theme_image("/image/icon-testimony.png"); ?>" alt="Ícone de Depoímento">
              </div>
              <div class="ht-testimonal__name">
                <span class="ht-testimonal__name--text">
                  <?php print $testimonal["ht_testimonal_name"]; ?>
                </span>
                <?php if(!empty($testimonal["ht_testimonal_sub"])): ?>
                  <span class="ht-testimonal__name--sub">
                    <?php print $testimonal["ht_testimonal_sub"]; ?>
                  </span>
                <?php endif; ?>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>

<?php endif; ?>
