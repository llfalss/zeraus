<?php
$p = get_queried_object();
$title = get_field("ht_home_about_title", $p);
$about = get_field("ht_home_about_text", $p);
$link = get_field("ht_home_about_url", $p);
$image = get_field("ht_home_about_image", $p);
if(!empty($about)):
?>
<div class="ht-section__double ht-section__double--desktop-text-first ht-home__about">
  <div class="ht-section__image ht-home__about__image">
    <div class="ht-section__wrapper--center">
      <img src="<?= $image["url"] ?>" alt="Logo de <?php print bloginfo("name") ?>">
    </div>
      
  </div>
  <div class="ht-section__text ht-home__about__text">
    <div class="ht-section__wrapper">
      <h2 class="ht-title ht-title__simple ht-about__title"><?= $title ?></h2>
      <div class="ht-text ht-text__wrapper ht-about__text">
        <?= wpautop($about); ?>
      </div>
      <?php if(!empty($link)): ?>
      <div class="ht-about__cta">
        <a href="<?= $link ?>" class="ht-button">Saiba mais <i class="fas fa-long-arrow-alt-right" style="margin-left:10px;"></i></a>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>
<?php endif; ?>
