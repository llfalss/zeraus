<?php
$p = get_queried_object();
$banners = get_field("ht_home_banners", $p);
if(!empty($banners)):
?>
<div class="ht-home-banner ht-home-banner--desktop ht-home-banner--js">
  <?php foreach($banners as $banner): ?>
    <div class="ht-home-banner__item ht-home-banner__item--desktop" style="background-image:url('<?php print $banner["ht_home_banner_image"]["url"] ?>')">
      <div class="ht-home-banner__content--desktop">

        <div class="ht-home-banner__wrapper--desktop">
          <h3 class="ht-home-banner__title ht-home-banner__title--desktop"><?php print $banner["ht_home_banner_title"]; ?></h3>
          <div class="ht-home-banner__text ht-home-banner__text--desktop">
            <?php print $banner["ht_home_banner_content"]; ?>
          </div>
          <?php if(!empty($banner["ht_home_banner_url"])): ?>
            <a href="<?php print $banner["ht_home_banner_url"] ?>" class="ht-button ht-home-banner__link ht-home-banner__link--desktop">
              <?php print $banner["ht_home_banner_cta"] ?> <i class="fas fa-long-arrow-alt-right" style="margin-left:10px;"></i>
            </a>
          <?php endif; ?>
        </div>

      </div>
    </div>
  <?php endforeach; ?>
</div>
<div class="ht-home-banner ht-home-banner--mobile ht-home-banner--js">
  <?php foreach($banners as $banner): ?>
    <div class="ht-home-banner__item ht-home-banner__item--mobile" style="background-image:url('<?php print $banner["ht_home_banner_image_mobile"]["url"] ?>')">
      <div class="ht-home-banner__overlay">
        <div class="ht-home-banner__content--mobile">
          <div class="ht-home-banner__wrapper--mobile">
            <h3 class="ht-home-banner__title ht-home-banner__title--mobile"><?php print $banner["ht_home_banner_title"]; ?></h3>
            <div class="ht-home-banner__text ht-home-banner__text--mobile">
              <?php print $banner["ht_home_banner_content"]; ?>
            </div>
            <?php if(!empty($banner["ht_home_banner_url"])): ?>
              <a href="<?php print $banner["ht_home_banner_url"] ?>" class="ht-button ht-home-banner__link ht-home-banner__link--desktop">
                <?php print $banner["ht_home_banner_cta"] ?> <i class="fas fa-long-arrow-alt-right" style="margin-left:10px;"></i>
              </a>
            <?php endif; ?>
          </div>
        </div>
      </div>

    </div>
  <?php endforeach; ?>
</div>
<?php endif; ?>
