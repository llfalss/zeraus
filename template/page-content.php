<?php $p = get_queried_object(); ?>
<div class="ht-section__single">
    <div class="ht-single__wrapper">
        <h1 class="ht-title"><?= $p->post_title; ?></h1>
        <div class="ht-text"><?= wpautop(do_shortcode($p->post_content)); ?></div>
    </div>
</div>