<?php
$p = get_queried_object();
$post = [
  "content" => $p->post_content,
  "image" => get_field("ht_post_image", $p),
];

if(!empty($post["content"])):
?>
<div class="ht-post">
  <div class="ht-post__container">
    <?php if(!empty($post["image"]["url"])): ?>
    <div class="ht-image__wrapper ht-image__wrapper--js">
      <a
      href="<?php print $post["image"]["url"] ?>"
      class="ht-post__image"
      style="background-image:url('<?php print $post["image"]["sizes"]["large"]; ?>')"
      >
      Imagem destacada
      </a>
    </div>
    <?php endif; ?>
    <h1 class="ht-title ht-post__title"><?php print $p->post_title; ?></h1>
    <div class="ht-post__header">
      <div class="ht-post__author">
        Postado por <?php print get_the_author_meta("display_name", $p->post_author) ?>
      </div>
      <div class="ht-post__date">
        <?php print ucfirst(get_the_date("l , d/m/Y", $p->ID )); ?>
      </div>
    </div>
    <div class="ht-post__content">
      <?php print wpautop(do_shortcode($post["content"])); ?>
    </div>
    
  </div>
</div>
<?php endif; ?>
