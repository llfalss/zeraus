<?php
$p = get_queried_object();
$link_voltar = ht_get_last_page();
?>
<section class="ht-produto">
  <div class="ht-produto__back">
    <a href="<?php print $link_voltar; ?>" class="ht-back__item">
      <i class="fas fa-arrow-left" style="margin-right:15px"></i> Voltar
    </a>
  </div>
  <div class="ht-produto__content">
    <?php get_template_part("template/produto","conteudo") ?>
  </div>
  <div class="ht-produto__galeria">
    <?php get_template_part("template/produto","galeria") ?>
  </div>
  <div class="ht-produto__descricao">
    <?php get_template_part("template/produto","descricao") ?>
  </div>
</section>
