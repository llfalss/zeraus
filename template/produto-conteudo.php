<?php
$p = get_queried_object();
$content = ht_get_product();
?>
<div class="ht-product__wrapper">
  <h1 class="ht-title ht-produto__title"><?php print $content["title"]; ?></h1>
  <div class="ht-produto__texto ht-text">
    <?php print wpautop($content["descricao"]); ?>
  </div>
  <form method="post">
    <div class="ht-produto__control">
      <div class="ht-control__item">
        <a href="<?=ht_get_wpp()["url"] .  "&text=" . get_permalink($p); ?>" class="ht-button ht-produtos__button ht-produtos__button--submit">
          Solicitar <i class="fas fa-long-arrow-alt-right" style="margin-left:15px"></i>
        </a>
      </div>

      <div class="ht-control__item<?php if(!empty($content["options"])): ?> ht-control__item--full<?php endif; ?>">
        <span class="ht-control__alert">Consulte todas informações com um de nossos atendente</span>
      </div>
    </div>
  </form>
  <div class="ht-control__details">
    <?php if(!empty(get_field("ht_product_code", $post))): ?>
    <div class="ht-details__item">
      Código: <?= get_field("ht_product_code", $post); ?>
    </div>
    <?php endif; ?>
    <div class="ht-details__item">
      Categoria: <?php
        $term_obj_list = get_the_terms( $post->ID, 'categoria' );
        if($term_obj_list)
          print join(', ', wp_list_pluck($term_obj_list, 'name'));
      ?>
    </div>
    <div class="ht-details__item">
      Marca: <?php
        $term_obj_list = get_the_terms( $post->ID, 'marca' );
        if($term_obj_list)
          print join(', ', wp_list_pluck($term_obj_list, 'name'));
      ?>
    </div>
  </div>
</div>
