<?php
$p = get_queried_object();
$content = ht_get_product();
if($content["descricao"] || $content["video"]):
?>
<div class="ht-produto__descricao">
  <div class="ht-descricao__wrapper">
    <?php if($content["video"]): ?>
      <div class="ht-descricao__item ht-descricao__video">
        <?php print $content["video"]; ?>
      </div>
    <?php endif; ?>
    <?php if($content["descricao"]): ?>
      <div class="ht-descricao__item ht-descricao__conteudo ht-text">
        <?php print wpautop($content["content"]); ?>
      </div>
    <?php endif; ?>
  </div>
</div>
<?php endif; ?>
