<?php
$p = get_queried_object();
$content = ht_get_product();

if($content["galeria"]):
?>
<div class="ht-galeria__wrapper ht-gallery--js">
  <?php foreach($content["galeria"] as $i => $galeria): ?>
    <a
    href="<?php print $galeria["url"] ?>"
    class="ht-galeria__item<?php
      if($i > 0) print " ht-galeria__item--thumb";
      if($i > 3) print " ht-galeria__item--hide";
    ?>"
    style="background-image:url('<?php print $galeria["url"]; ?>')"
    >
      <?php if($i == 3 && count($content["galeria"]) > 4 ): ?>
        <div class="ht-galeria__overlay">
          <i class="fas fa-plus"></i>
        </div>
      <?php endif; ?>
      <img
      src="<?php print $galeria["sizes"]["thumbnail"] ?>"
      alt="<?php print $galeria["alt"] ?>"
      class="ht-galeria__thumb">
    </a>
  <?php endforeach; ?>
</div>
<?php else: ?>
  <div class="ht-galeria__wrapper--vazio">

  </div>
<?php endif; ?>
