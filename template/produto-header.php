<?
    $args = array(
        "post_type" => "linha",
        "orderby"=>"title",
        "order"=>"asc"
    );
   $query = get_posts($args );
    
   $title = get_field("ht_product-page-title");
   $text = get_field("ht_product-page-text");
  
   
?>

<div class="ht-produto-header">
    <div>
        <h1><? print $title ?></h1>
        <p><? print $text ?></p>
    </div>
    <div class="ht-produto-list">
        <? 
           foreach ($query as $post):
        ?>
        <a href="<? print get_permalink() ?>" class="ht-produto-item" style="background-image: linear-gradient(180deg, rgba(0, 38, 59, 0) 23.96%, #00263B 65.62%),url(<? print get_field('ht_linha-img') ?>);"><h1><? print $post->post_title ?></h1></a>
        <?
            endforeach;
        ?>
        
    </div>
</div>