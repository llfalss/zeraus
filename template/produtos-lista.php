<?php
// $posts = new ht_post_group("produto");
// $posts->set_arg("orderby", "name");
// $posts->set_arg("order", "ASC");
// if($_GET["s"])
//   $posts->set_arg("s", $_GET["s"]);
// $posts->set_number_post(-1);
$posts = ht_get_products();
$wpp = ht_get_wpp();

if(!empty($posts) && count($posts) > 0):
?>
<div class="ht-produtos__wrapper">
  <?php foreach($posts as $p): ?>
    <?php $produto_atual = ht_get_product($p); ?>
    <div class="ht-produtos__item">
      <div class="ht-produtos-item__wrapper">
        <a
        href="<?php print get_permalink($p); ?>"
        class="ht-produtos-item__image"
        <?php if(!empty($produto_atual["galeria"][0]["sizes"]["large"])): ?>
          style="background-size: cover; background-image:url('<?php print $produto_atual["galeria"][0]["sizes"]["large"] ?>')"
        <?php endif; ?>
        >
          <div class="ht-produtos-item__image--saibamais">
            <div class="ht-produtos-item__image--buttom">
              Saiba mais
            </div>
          </div>
        </a>
        <h3 class="ht-produtos__title"><?php print $produto_atual["title"]; ?></h3>
        <!-- <a href="<?=ht_get_wpp()["url"] .  "&text=" . get_permalink($p); ?>" class="ht-produtos__button">Solicitar</a> -->
        <a href="<?= get_permalink($p); ?>" class="ht-button ht-produtos__button">Saiba mais</a>

      </div>
    </div>
  <?php endforeach; ?>
</div>
<?php else: ?>

<?php endif; ?>
