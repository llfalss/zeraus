<?php
$menu = ht_get_taxonomies_list();
?>
<!-- Desktop -->
<div class="ht-produtos-sidebar__wrapper ht-produtos-sidebar__wrapper--desktop">
  <form method="get" class="ht-produtos-sidebar__wait--js">
    <?php foreach($menu as $m): ?>
    <?php if($m["list"]): ?>
    <div class="ht-produtos-sidebar__group">
      <div class="ht-produtos-sidebar__title">
        <?php print $m["name"] ?>
      </div>
      <div class="ht-produtos-sidebar__list">
          <?php foreach($m["list"] as $s): ?>
            <div class="ht-produtos-sidebar__item<?php if($s->parent > 0) print " ht-produtos-sidebar__item--parent" ?>">
              <label class="ht-register__check" style="cursor:pointer">
                <input type="checkbox" value="<?php print $s->term_id ?>" name="filtro[<?php print $m["slug"]; ?>][]"<?php
                  if(is_array($_GET["filtro"][$m["slug"]]))
                  {
                    if(in_array($s->term_id, $_GET["filtro"][$m["slug"]]))
                      print " checked";
                  }
                ?>>
                <span class="ht-register__check--checkmark"></span>
                <?php print $s->name; ?>
              </label>
            </div>

          <?php endforeach; ?>
      </div>
    </div>
    <?php endif; ?>
    <?php endforeach; ?>
  </form>
</div>
<!-- /Desktop -->
<!-- Mobile -->
<form method="get">
  <div class="ht-produtos-sidebar__wrapper ht-produtos-sidebar__wrapper--mobile">
    <div class="ht-produtos-sidebar__close ht-produtos-sidebar__show--js">
      <i class="fas fa-times" style="margin-right:15px;"></i> Fechar
    </div>
    <?php foreach($menu as $m): ?>
    <div class="ht-produtos-sidebar__group">
      <div class="ht-produtos-sidebar__title">
        <?php print $m["name"] ?>
      </div>
      <div class="ht-produtos-sidebar__list">
          <?php foreach($m["list"] as $s): ?>
            <div class="ht-produtos-sidebar__item<?php if($s->parent > 0) print " ht-produtos-sidebar__item--parent" ?>">
              <label class="ht-register__check">
                <input type="checkbox" value="<?php print $s->term_id ?>" name="filtro[<?php print $m["slug"]; ?>][]"<?php
                  if(is_array($_GET["filtro"][$m["slug"]]))
                  {
                    if(in_array($s->term_id, $_GET["filtro"][$m["slug"]]))
                      print " checked";
                  }
                ?>>
                <span class="ht-register__check--checkmark"></span>
                <?php print $s->name; ?>
              </label>
            </div>

          <?php endforeach; ?>
      </div>
    </div>
  <?php endforeach; ?>
  </div>
  <div class="ht-produtos-sidebar__submit">
    <button type="submit" class="ht-produtos-sidebar__submit--item">
      <i class="fas fa-paper-plane" style="margin-right:15px;"></i> Pesquisar
    </button>
  </div>
</form>
<a href="#" class="ht-button ht-produtos-sidebar__show ht-produtos-sidebar__show--js">
  <i class="fas fa-filter" style="margin-right:15px;"></i> Filtros
</a>
<!-- /Mobile -->
<div class="ht-produtos-sidebar__wait">
  <i class="fas fa-cog fa-spin"></i>
</div>
