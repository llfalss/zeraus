<?
    $config = get_field("ht_vagas-config");

    $args = array(
        "post_type" => "vaga"
    );
    $posts = query_posts($args);

?>

<div class="ht_trabalhe-conosco-wrapper">
    <h1><? print $config["ht_title"] ?></h1>
    <p><? print $config["ht_text"] ?> 
        <? if(!empty($config["ht_mail"])):?>
            <br> Enviar currículo para: <span><? print $config["ht_mail"] ?></span>
        <? endif;?>
    </p>
    <div>
        <? foreach ($posts as $post):
            $postDetail = get_field("detalhes_da_vaga")    
        ?>
            
        <div>
            <h1><? print $postDetail["ht_title"] ?></h1>
            <p><? print $postDetail["ht_text"] ?></p>
        </div>
        <? endforeach; ?>
    </div>
</div>